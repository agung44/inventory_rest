package models

import (
  "github.com/jinzhu/gorm"
  "time"
)

type IncomingProduct struct {
  gorm.Model
  Time time.Time
  Product Product
  ProductID int `sql:"not null"`
  OrderAmount int `sql:"not null"`
  ReceivedAmount int `sql:"not null"`
  PurchasePrice float64 `sql:"not null"`
  TotalPrice float64 `sql:"not null"`
  ReceiptNo string `sql:"size:255;not null"`
  Notes string `sql:"size:255;not null"`
}
