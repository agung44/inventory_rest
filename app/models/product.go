package models

import (
  "github.com/jinzhu/gorm"
)

type Product struct {
  gorm.Model
  Sku string `sql:"size:255;not null"`
  Name string `sql:"size:255;not null"`
  Quantity int `sql:"default 0"`
}

func FindAllProducts()(products []Product){
  DB.Find(&products)
  return
}
