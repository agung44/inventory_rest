package models

import (
  "github.com/jinzhu/gorm"
  "time"
)

type SalesReport struct {
  gorm.Model
  Time time.Time
  Product Product
  ProductID int `sql:"not null"`
  OrderID string `sql:"size:255;not null"`
  Quantity int `sql:"default 0"`
  SellingPrice float64 `sql:"not null"`
  TotalPrice float64 `sql:not null"`
  PurchasePrice float64 `sql:not null"`
  Profit float64 `sql:not null"`
}
