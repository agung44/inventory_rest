package models

import (
  "github.com/jinzhu/gorm"
)

type InventoryValuesReport struct {
  gorm.Model
  Product Product
  ProductID int `sql:"not null"`
  Quantity int `sql: "not null"`
  AveragePurchasePrice float64 `sql: not null`
  TotalPrice float64 `sql not null`
}
