package models

import (
  "github.com/jinzhu/gorm"
  "time"
)

type OutgoingProduct struct {
  gorm.Model
  Time time.Time
  Product Product
  ProductID int `sql:"not null"`
  OrderID string `sql:"size:255;not null"`
  ReleasedAmount int `sql:"not null"`
  SellingPrice float64 `sql:"not null"`
  TotalPrice float64 `sql:"not null"`
  Notes string `sql:"size:255;not null"`
}
