package models

import (
  _ "github.com/jinzhu/gorm/dialects/postgres"
  "github.com/jinzhu/gorm"
  "github.com/revel/revel"
  "fmt"
  "log"
  "os"
)

var db *gorm.DB
var DB *gorm.DB

func Init() {
  adapter := revel.Config.StringDefault("db.adapter", "sqlite3")
  databaseUri := revel.Config.StringDefault("db.uri", "")
  fmt.Printf("** adapter %s; uri %s", adapter, databaseUri)
  var err error
  db, err := gorm.Open(adapter, databaseUri)
  DB = db
  if err != nil {
    panic(err)
  }

  // defer db.Close()

  db.LogMode(true)
  db.SetLogger(log.New(os.Stdout, "  ", 0))
  db.AutoMigrate(&Product{})
  db.AutoMigrate(&IncomingProduct{})
  db.AutoMigrate(&OutgoingProduct{})
  db.AutoMigrate(&InventoryValuesReport{})
  db.AutoMigrate(&SalesReport{})

  product := Product{Sku: "asd123", Name: "Shoes", Quantity: 19}
  db.Create(&product)
}
