package controllers

import (
  "github.com/revel/revel"
  "inventory_rest/app/models"
)

type ProductController struct {
  *revel.Controller
}

type Response struct {
  Status int
  Message string
}

func (c ProductController) Index() revel.Result {
  c.Response.Status = 200
  response := JsonResponse{}
  products := models.FindAllProducts()
  response.Data = products
  return c.RenderJSON(response)
}

func (c ProductController) Create() revel.Result {
  response := JsonResponse{}
  response.Data = Response{Status: 200, Message: "Successfully create a Product"}
  return c.RenderJSON(response)
}
//
// func (c ProductController) Destroy() revel.Result {
// }
